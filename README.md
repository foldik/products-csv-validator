# Product Csv Validator

Először is, gratulálok, ha ebben a feladatban bármeddig is eljutottál. Ez abszolút nem volt egy könnyű feladat, így ha akár ilyen szintű feladatokkal is meg tudsz birkózni, akkor nem lesz félnivalód ebben a szakmában :slight_smile:

A továbbiakban leírom, hogyan is érdemes egy ilyen alkalmazásnak neki állni, milyen lépésekben érdemes haladni.

### Megoldás

__Egy csv fájlt kell beolvasni, amire az alábbi validációs szabályokat kell alkalmazni__

- Egy fájlban nem lehet több 2000 terméknél, de ez az érték konfigurálható kell, hogy legyen.
- Minden oszlopban kötelezőp megadni értéket a megfelelő típusban.
- Nem lehet két product ugyanazzal az azonosítóval (Id-val).
- Nem vehetünk fel 100000 Ft azaz százezer forintnál drágább terméket, de ez is legyen  konfigurálható.

__Na már most ezek után milyen kép állhat össze a fejünkben a validálásról, felmerül-e valamilyen kérdés?__

- Validáljuk-e a header-t? - Igen, legyen a válasz igen és legyen az értéke fixen ```Id,Product name,Price,Category```
- Milyen típusú validációk lesznek, milyen üzeneteket adhatok vissza?
 - Lesz olyan, ami csak egyedi sorokra igaz. Ilyen lesz például a __Header__, __Product name__, __Price__, __Category__ validációja, hiszen itt az adott sor érvényessége nem függ egy másik sortól, mint példuál az __Id__ esetében később.
 - Lesz olyan, ami több sort is érint egyszerre. Így példul a __maximális termék szám__ és az __Id__ validációjánál már a többi sort is figyelembe kell vennnünk.

...ezekből a megfontolásokból a lehetséges hibaüüzeneteket

- ```Product csv file must be at least two lines long. One for the header and one for the product.``` - ha a fájl nem tartalmaz legalább 2 sort
- ```Header must be: Id,Product name,Price,Category``` - ha a header nem lenne megfelelő
- ```Maximum product 2000``` - ha túl lépnénk a maximálisan megengedett termékek számát
- ```Invalid id at line 200``` - ha egy nem szám értéket találnánk a 200-ik sorban
- ```Multiple occurrence of 12345 id``` - ha egy azonosító többször is előfordul
- ```Missing product name at line 200``` - ha hiányozna a termék név
- ```Invalid content [] at line 500``` - ha egy sorban semmi se lenne, vagy például category, esetleg szám nem lenne átalakítható
- ```Too big price 1000000 at line 213``` - ha egy sorban termék ára túl nagy lenne


__Milyen kérdést tehetünk fel a domain struktúráról, milyen model osztályokat használjunk?__

- Rögtön a beolvasásnál látszik, hogy jól jönne egy ```CsvFile``` osztály, ami tartalmaz egy ```String header```-t és egy ```List<Line> lines```-t. A ``Line`` osztály azért lesz hasznos, mert ebben tárolhatjuk a sor tartalmát és a sor számát, ami majd a hibaüzenetekhez lesz hasznos.
- A kivétel kezeléséhez jól jönne egy saját Exception ```InvalidProductCsvException``` néven, ami tartalmazza a hibákat.
- A termék remprezentálására egy ```Product``` és egy ```Category``` osztály és enum.

__Hogyan struktúráljuk a komponenseinket, hogyan építsük fel a spring-es alkalmazásunkat?__
- Spring-ről lévén szó, egy futtató ```Main``` osztály, egy java konfigot tartalmazó ```AppConfig``` osztály és egy property-ket tartalmazó ```src/main/resources/application.properties``` fájl kötelező.
- A validációs szabályok elég összetettek és sok van belőlük, így nem érdemes mindet egy osztályba rakni, érdemes őket szétbontani a következőkre:
 - ```ProductCsvValidator``` - beolvassa a fájlt és végig hív a lenti különböző alacsonyabb szintű validátorokon (ezek neve elég beszédes, ezért nem kommentelem tovább), mindegyik kisebb validátor visszaadja a hibákat amiket talált, vagy csak egy üres listát, ha minden király
 - ```HeaderValidator```
 - ```ProductCountValidator```
 - ```IdValidator```
 - ```ProductNameValidator```
 - ```PriceValidator```
 - ```CategoryValidator```

### Tesztelés

Az alkalmazás tartalmaz néhány teszt fájlt az ```src/test/resources``` mappa alatt. Jó szokás a tesztekhez szükséges fájljainkat ide rakni __Maven__-es projektek esetén, úgyhogy ezt se felejtsük el.

Az STS elképzelhető, hogy nem írja ki a sok hibát, legalábbis összevonja őket. Ezen az alábbi gomb megnyomásával segíthetünk.

![sts-help.png](images/sts-help.png)

__Valid fájl esetén az alábbi eredményt kapjuk :slight_smile:__

```
Success. src/test/resources/validProducts.csv was valid
```
