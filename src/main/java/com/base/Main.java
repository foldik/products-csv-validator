package com.base;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.base.config.AppConfig;
import com.base.validate.ProductCsvValidator;

public class Main {
	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

		ProductCsvValidator productCsvValidator = context.getBean(ProductCsvValidator.class);

		String fileName = "src/test/resources/validProducts.csv";

		productCsvValidator.validateFile(fileName);

		System.out.println("Success. " + fileName + " was valid");
	}
}
