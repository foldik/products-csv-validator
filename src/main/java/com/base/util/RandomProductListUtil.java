package com.base.util;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.base.model.Category;
import com.base.model.Product;

public class RandomProductListUtil {

	private RandomProductListUtil() {

	}

	public static List<Product> generateRandomProducts(int numberOfProducts) {
		return IntStream.range(0, numberOfProducts)
				.mapToObj(i -> {
					return new Product(i, randomProductName(i), randomPrice(), randomCetagory());
				}).collect(Collectors.toList());
	}

	private static String randomProductName(long i) {
		return "Product-" + i;
	}

	private static long randomPrice() {
		return ThreadLocalRandom.current().nextLong(99999);
	}

	private static Category randomCetagory() {
		double c = ThreadLocalRandom.current().nextDouble();
		if (c < 0.25) {
			return Category.BOOK;
		} else if (c < 0.5) {
			return Category.FOOD;
		} else if (c < 0.75) {
			return Category.DRINK;
		} else {
			return Category.GAME;
		}
	}

}
