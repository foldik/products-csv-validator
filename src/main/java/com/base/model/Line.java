package com.base.model;

import java.util.Objects;

public final class Line {

	private final int lineNumber;
	private final String content;

	public Line(int lineNumber, String content) {
		if (lineNumber < 0) {
			throw new IllegalArgumentException("lineNumber must be greater than zero, actual is " + lineNumber);
		}
		this.lineNumber = lineNumber;
		this.content = Objects.requireNonNull(content, "content must not be null");
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public String getContent() {
		return content;
	}

	@Override
	public String toString() {
		return "Line [lineNumber=" + lineNumber + ", content=" + content + "]";
	}
}
