package com.base.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public final class CsvFile {

	private final String header;
	private final List<Line> lines;

	public CsvFile(String header, List<Line> lines) {
		this.header = Objects.requireNonNull(header, "header must not be null");
		this.lines = Collections.unmodifiableList(new ArrayList<>(lines));
	}

	public String getHeader() {
		return header;
	}

	public List<Line> getLines() {
		return lines;
	}

	@Override
	public String toString() {
		return "CsvFile [header=" + header + ", lines=" + lines + "]";
	}

}
