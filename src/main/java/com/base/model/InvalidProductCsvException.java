package com.base.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class InvalidProductCsvException extends RuntimeException {

	private final List<String> errorCauses;

	public InvalidProductCsvException(List<String> errorCause) {
		super(errorCause.toString());
		this.errorCauses = Collections.unmodifiableList(new ArrayList<>(errorCause));
	}

	@Override
	public String toString() {
		return "InvalidProductCsv [errorCause=" + errorCauses + "]";
	}

	public List<String> getErrorCauses() {
		return errorCauses;
	}

}
