package com.base.model;

public enum Category {

	GAME("Game"), BOOK("Book"), FOOD("Food"), DRINK("Drink");

	private final String name;

	private Category(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public static Category findCategoryByName(String value) {
		for (Category category : values()) {
			if (category.name.equals(value)) {
				return category;
			}
		}
		throw new IllegalArgumentException(value);
	}

}
