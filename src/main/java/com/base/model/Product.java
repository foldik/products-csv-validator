package com.base.model;

public final class Product {

	private final long id;
	private final String productName;
	private final long price;
	private final Category category;

	public Product(long id, String productName, long price, Category category) {
		this.id = id;
		this.productName = productName;
		this.price = price;
		this.category = category;
	}

	public long getId() {
		return id;
	}

	public String getProductName() {
		return productName;
	}

	public long getPrice() {
		return price;
	}

	public Category getCategory() {
		return category;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", productName=" + productName + ", price=" + price + ", category=" + category
				+ "]";
	}

}
