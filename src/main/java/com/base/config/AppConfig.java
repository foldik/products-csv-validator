package com.base.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import com.base.validate.CategoryValidator;
import com.base.validate.HeaderValidator;
import com.base.validate.IdValidator;
import com.base.validate.PriceValidator;
import com.base.validate.ProductCountValidator;
import com.base.validate.ProductCsvValidator;
import com.base.validate.ProductNameValidator;

@Configuration
@ComponentScan(basePackages = "com.base")
@PropertySource("classpath:application.properties")
public class AppConfig {

	@Autowired
	private Environment env;

	@Bean
	public ProductCsvValidator productCsvValidator() {
		return new ProductCsvValidator(headerValidator(), productCountValidator(), idValidator(),
				productNameValidator(), priceValidator(), categoryValidator());
	}

	@Bean
	public HeaderValidator headerValidator() {
		return new HeaderValidator();
	}

	@Bean
	public ProductCountValidator productCountValidator() {
		return new ProductCountValidator(env.getRequiredProperty("produtcs.csv.maxProductCount", Integer.class));
	}

	@Bean
	public IdValidator idValidator() {
		return new IdValidator(env.getRequiredProperty("produtcs.csv.separator"));
	}

	@Bean
	public ProductNameValidator productNameValidator() {
		return new ProductNameValidator(env.getRequiredProperty("produtcs.csv.separator"));
	}

	@Bean
	public PriceValidator priceValidator() {
		return new PriceValidator(env.getRequiredProperty("produtcs.csv.separator"),
				env.getRequiredProperty("produtcs.csv.maxProductPrice", Integer.class));
	}

	@Bean
	public CategoryValidator categoryValidator() {
		return new CategoryValidator(env.getRequiredProperty("produtcs.csv.separator"));
	}

}
