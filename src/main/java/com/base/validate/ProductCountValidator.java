package com.base.validate;

import static java.util.Collections.emptyList;

import java.util.Arrays;
import java.util.List;

import com.base.model.Line;

public class ProductCountValidator {

	private final int maxProductCount;

	public ProductCountValidator(int maxProductCount) {
		this.maxProductCount = maxProductCount;
	}

	public List<String> validate(List<Line> lines) {
		if (lines.size() > maxProductCount) {
			return Arrays.asList("Maximum product " + maxProductCount);
		}
		return emptyList();
	}

}
