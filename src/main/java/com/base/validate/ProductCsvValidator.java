package com.base.validate;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import com.base.model.CsvFile;
import com.base.model.InvalidProductCsvException;
import com.base.model.Line;

public class ProductCsvValidator {

	private final HeaderValidator headerValidator;
	private final ProductCountValidator productCountValidator;
	private final IdValidator idValidator;
	private final ProductNameValidator productNameValidator;
	private final PriceValidator priceValidator;
	private final CategoryValidator categoryValidator;

	public ProductCsvValidator(HeaderValidator headerValidator,
							   ProductCountValidator productCountValidator, 
							   IdValidator idValidator,
							   ProductNameValidator productNameValidator, 
							   PriceValidator priceValidator,
							   CategoryValidator categoryValidator) {
		this.headerValidator = Objects.requireNonNull(headerValidator, "headerValidator must not be null");
		this.productCountValidator = Objects.requireNonNull(productCountValidator, "productCountValidator must not be null");
		this.idValidator = Objects.requireNonNull(idValidator, "idValidator must not be null");
		this.productNameValidator = Objects.requireNonNull(productNameValidator, "productNameValidator must not be null");
		this.priceValidator = Objects.requireNonNull(priceValidator, "priceValidator must not be null");
		this.categoryValidator = Objects.requireNonNull(categoryValidator, "priceValidator must not be null");
	}

	public void validateFile(String fileName) {

		try {
			List<String> errorCauses = new ArrayList<>();

			CsvFile csvFile = readCsvFile(Files.readAllLines(Paths.get(fileName)));

			errorCauses.addAll(headerValidator.validate(csvFile.getHeader()));
			errorCauses.addAll(productCountValidator.validate(csvFile.getLines()));
			errorCauses.addAll(idValidator.validate(csvFile.getLines()));
			errorCauses.addAll(productNameValidator.validate(csvFile.getLines()));
			errorCauses.addAll(priceValidator.validate(csvFile.getLines()));
			errorCauses.addAll(categoryValidator.validate(csvFile.getLines()));

			if (!errorCauses.isEmpty()) {
				throw new InvalidProductCsvException(errorCauses);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

	}

	private CsvFile readCsvFile(List<String> linesFromFile) {
		if (linesFromFile.size() < 2) {
			throw new InvalidProductCsvException(Arrays.asList(
					"Product csv file must be at least two lines long. One for the header and one for the product."));
		}
		List<Line> lines = new ArrayList<>();
		for (int i = 1; i < linesFromFile.size(); i++) {
			lines.add(new Line(i, linesFromFile.get(i)));
		}
		return new CsvFile(linesFromFile.get(0), lines);
	}
}
