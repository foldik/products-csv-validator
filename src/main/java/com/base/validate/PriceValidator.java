package com.base.validate;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.base.model.Line;

public class PriceValidator {

	private final String separator;
	private final int maxProductPrice;

	public PriceValidator(String separator, int maxProductPrice) {
		this.separator = Objects.requireNonNull(separator, "separator must not be null");
		this.maxProductPrice = maxProductPrice;
	}

	public List<String> validate(List<Line> lines) {
		List<String> errorCauses = new ArrayList<>();

		for (Line line : lines) {
			String[] values = line.getContent().split(separator);
			String lineValidationResult;
			try {
				Long price = Long.valueOf(values[2]);
				if (price > maxProductPrice) {
					lineValidationResult = "Too big price " + price + " at line " + line.getLineNumber();
				}
				lineValidationResult = "";
			} catch (IllegalArgumentException e) {
				lineValidationResult = "Invalid content [" + line.getContent() + "] at line " + line.getLineNumber();
			}
			if (lineValidationResult.length() != 0) {
				errorCauses.add(lineValidationResult);
			}
		}
		return errorCauses;
	}

}
