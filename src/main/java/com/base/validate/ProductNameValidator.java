package com.base.validate;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.base.model.Line;

public class ProductNameValidator {

	private final String separator;

	public ProductNameValidator(String separator) {
		this.separator = Objects.requireNonNull(separator, "separator must not be null");
	}

	public List<String> validate(List<Line> lines) {
		List<String> errorCauses = new ArrayList<>();

		for (Line line : lines) {
			String[] values = line.getContent().split(separator);
			String lineValidationResult;
			try {
				String productName = values[1];
				if (productName == null || productName.length() == 0) {
					lineValidationResult = "Missing product name at line " + line.getLineNumber();
				} else {
					lineValidationResult = "";
				}

			} catch (Exception e) {
				lineValidationResult = "Invalid content [" + line.getContent() + "] at line " + line.getLineNumber();
			}
			if (lineValidationResult.length() != 0) {
				errorCauses.add(lineValidationResult);
			}
		}
		return errorCauses;
	}
}
