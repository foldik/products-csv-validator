package com.base.validate;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.base.model.Category;
import com.base.model.Line;

public class CategoryValidator {

	private final String separator;

	public CategoryValidator(String separator) {
		this.separator = Objects.requireNonNull(separator, "separator must not be null");
	}

	public List<String> validate(List<Line> lines) {
		List<String> errorCauses = new ArrayList<>();

		for (Line line : lines) {
			String[] values = line.getContent().split(separator);
			String lineValidationResult;
			try {
				Category.findCategoryByName(values[3]);
				lineValidationResult = "";
			} catch (IllegalArgumentException e) {
				lineValidationResult = "Invalid content [" + line.getContent() + "] at line " + line.getLineNumber();
			}
			if (lineValidationResult.length() != 0) {
				errorCauses.add(lineValidationResult);
			}
		}
		return errorCauses;
	}

}
