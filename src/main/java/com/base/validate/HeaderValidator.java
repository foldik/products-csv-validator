package com.base.validate;

import static java.util.Collections.emptyList;

import java.util.Arrays;
import java.util.List;

public class HeaderValidator {
	
	private static final String VALID_HEADER = "Id,Product name,Price,Category";
	
	public List<String> validate(String header) {
		String headerwithoutWhiteSpace = header.trim();
		if (!headerwithoutWhiteSpace.equals(VALID_HEADER)) {
			return Arrays.asList("Header must be: " + VALID_HEADER);
		}
		return emptyList();
	}

}
