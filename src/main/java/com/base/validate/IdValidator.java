package com.base.validate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.base.model.Line;

public class IdValidator {

	private final String separator;

	public IdValidator(String separator) {
		this.separator = Objects.requireNonNull(separator, "separator must not be null");
	}
	
	public List<String> validate(List<Line> lines) {
		List<String> errorCauses = new ArrayList<>();

		// Try to group lines by id-s, if there are more Line in one group then it means
		// a problem
		Map<Long, List<Line>> productsGrouppedByIds = new HashMap<>();
		for (Line line : lines) {
			String[] values = line.getContent().split(separator);
			try {
				Long id = Long.parseLong(values[0]);
				if (productsGrouppedByIds.containsKey(id)) {
					productsGrouppedByIds.get(id).add(line);
				} else {
					productsGrouppedByIds.put(id, Arrays.asList(line));
				}
			} catch (Exception e) {
				errorCauses.add("Invalid id at line " + line.getLineNumber());
			}
		}

		// Check whether there was a problem or not
		for (Map.Entry<Long, List<Line>> productGrouppedByIds : productsGrouppedByIds.entrySet()) {
			if (productGrouppedByIds.getValue().size() > 1) {
				errorCauses.add("Multiple occurrence of " + productGrouppedByIds.getKey() + " id");
			}
		}
		return errorCauses;
	}

}
