package com.base;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

import com.base.model.Product;

public class ProductCsvFileWriter {

	private final String separator;

	public ProductCsvFileWriter(String separator) {
		this.separator = Objects.requireNonNull(separator, "separator must not be null");
	}

	public void writeToFile(List<Product> products, String fileName) {
		StringBuilder content = new StringBuilder();
		content.append("Id" + separator + "Product name" + separator + "Price" + separator + "Category");
		for (Product product : products) {
			content.append(System.lineSeparator());
			content.append(product.getId());
			content.append(separator);
			content.append(product.getProductName());
			content.append(separator);
			content.append(product.getPrice());
			content.append(separator);
			content.append(product.getCategory().getName());
		}
		try {
			Files.write(Paths.get(fileName), content.toString().getBytes(StandardCharsets.UTF_8));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
